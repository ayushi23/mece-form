import React, { Component } from 'react';
import ReactGA from 'react-ga';
import $ from 'jquery';
import './App.css';
import Search from './Components/Search';
import Header from './Components/Header';
import Reg from './Components/Reg';
import Sidebar from './Components/Sidebar';
import Footer from './Components/Footer';
import Companydetail from './Components/Companydetail';
import Demo from './Components/Demo';
import About from './Components/About';
import Blog from './Components/Blog';
import Contact from './Components/Contact';
import Signup from './Components/Signup';
import Login from './Components/Login';
import Home from './Components/Home';
import Reference from './Components/Reference';
import Try from './Components/Try';
import Terms from './Components/Terms';
import { BrowserRouter as Router, Route,Link } from 'react-router-dom';
import {Navbar, Form, Col, FormGroup, ControlLabel, FormControl, Button, Nav, MenuItem, NavDropdown, NavItem, Image} from 'react-bootstrap';
import SocialMediaIcons from 'react-social-media-icons';
import { InputGroup, InputGroupText, InputGroupAddon, Input } from 'reactstrap';
//import Autocomplete from './Components/Autocomplete';
// import Header from './Components/Header';
// import Footer from './Components/Footer';
// import About from './Components/About';
// import Resume from './Components/Resume';
// // import Search from './Components/Search';
// //import Contact from './Components/Contact';
// import Testimonials from './Components/Testimonials';
// import Portfolio from './Components/Portfolio';
import Main from './Components/Main';


const socialMediaIcons = [
  {
  url: 'https://linkedin.com',
  className: 'fab fa-linkedin-in footer-soc',
  },
  {
  url: 'https://twitter.com',
  className: 'fab fa-twitter footer-soc',
  },

];

class App extends Component {

  constructor(props){
    super(props);
    this.state = {
      foo: 'bar',
      contactName:'',
      contactEmail:'',
      contactSubject:'',
      contactMessage:'',
      resumeData: {}
    };

    ReactGA.initialize('UA-110570651-1');
    ReactGA.pageview(window.location.pathname);

  }

  handleChange (event) {
   console.log(event);
    this.setState( [event.target.name]: event.target.value )
  }

  getResumeData(){
    $.ajax({
      url:'/resumeData.json',
      dataType:'json',
      cache: false,
      success: function(data){
        this.setState({resumeData: data});
      }.bind(this),
      error: function(xhr, status, err){
        console.log(err);
        alert(err);
      }
    });
  }

  componentDidMount(){
    this.getResumeData();
  }

  render() {
    return (
          <Router>
      <div className="App">
        <Main/>

        <Navbar inverse collapseOnSelect fixedTop>
      <Navbar.Header>
        <Navbar.Brand>
           <Link to="/Home" ><Image src="./images/2.jpg" style={{width:'50px'}}/></Link>
        </Navbar.Brand>
        <Navbar.Toggle />
      </Navbar.Header>
      <Navbar.Collapse>
          <Nav className="main-nav" style={{fontWeight:'bold',color:'white',fontSize:'18px'}}>
            <NavItem eventKey={1}  >
             <Link to="/Sidebar" className="link">Explore</Link>
              
            </NavItem>
            <NavItem eventKey={2}>
              <Link to="/Home" className="link">Demo</Link>
            </NavItem>
            
            <NavItem eventKey={2}>
               <Link to="/About" className="link">About</Link>
            </NavItem>
            <NavItem eventKey={2}>
              <Link to="/Blog" className="link">Blog</Link>
            </NavItem>
            <NavItem eventKey={2} >
              <Link to="/Reg" className="link">Contact</Link>
            </NavItem>
          </Nav>
          <Nav pullRight style={{fontSize:'18px'}}>

            <NavItem eventKey={1} >
            
            <Image src="./images/key.png" style={{width:'19px'}} /> &nbsp; 
             <Link to="/Signup" className="link">Signup</Link>
            </NavItem>
            <NavItem eventKey={2}>
            <Image src="./images/login.png" style={{width:'19px'}} /> &nbsp;
              <Link to="/Login" className="link">Login</Link>
            </NavItem>
          </Nav>
        </Navbar.Collapse>
      </Navbar>

       
     
         <Route exact path="/" component={Home} />
         <Route path="/Sidebar" component={Sidebar} />
         <Route path="/Home" component={Home} />
         <Route path="/Companydetail" component={Companydetail} />
         <Route path="/About" component={About} />
         <Route path="/Blog" component={Blog} />
         <Route path="/Reg" component={Reg} />
         <Route path="/Login" component={Login} />
         <Route path="/Signup" component={Signup} />
         <Route path="/Try" component={Try} />
         <Route path="/Terms" component={Terms} />




   <div style ={{ backgroundImage: "url(./images/World-Map-Free-PNG-Image.png)" }} className="row footer-div">
    <div className = "container">
    <div class="col-md-3 footer-left-div">
      <Link to="/Home"><Image style={{height:'50px'}} className="logo" src="./images/2.jpg" /></Link>
      <p className="footer-left-div-p">Bld Mihail Kogalniceanu, <br /> nr 8, 7652 Bucharest, <br /> Texas</p>
    </div>
    <div class="col-md-8 footer-center-div">
    <div className="col-md-12">
      <div className="col-sm-2 col-xs-12 footer-menu">
      <Link to="/Try" className="link">Try it out</Link>&nbsp;
      </div>
      <div className="col-sm-2 col-xs-12 footer-menu">
        <Link to="/Home" className="link">Demo</Link>&nbsp;
      </div>
      <div className="col-sm-2 col-xs-12 footer-menu">
        <Link to="/About" className="link">About</Link>&nbsp;
      </div>
      <div className="col-sm-1 col-xs-12 footer-menu">
        <Link to="/Blog" className="link">Blog</Link>&nbsp;
      </div>
      <div className="col-sm-2 col-xs-12 footer-menu">
        <Link to="/Reg" className="link">Contact</Link>&nbsp;
      </div>
      <div className="col-sm-3 col-xs-12 footer-menu">
        <Link to="/Terms" className="link">Terms of services</Link>
      </div>
    </div>

    <div className="subscribe-div">
    <p> Subscribe Us </p>
    <InputGroup className="footer-email-group">
      <Input className="inpt-text footer-email" placeholder="Email Address" style={{color:'white'}}/>
      <InputGroupAddon addonType="append" style={{float:'right', marginTop: '-39px',padding:'2px 1px'}}>
      <InputGroupText className='btn btn-warning' style={{fontSize:'16px',borderTop: 'solid 1px #ec9926'}}><i class="fa fa-angle-right" aria-hidden="true"></i></InputGroupText>

      </InputGroupAddon>
    </InputGroup>
    </div>

    </div>

    <div class="col-md-1 footer-right-div">
    <div className="text-center">
    <p style={{color:'white',fontWeight:'bold',fontSize:'15px'}}> Follow Us </p>
    <SocialMediaIcons
    icons={socialMediaIcons}
    iconSize={'1.3em'}
    iconColor={'#495056'}
    />

    </div>
    </div>
    <div className='copyright-div text-center'>
    <p>Copyright 2017,MECE All Right Reserved </p>
    </div>
    </div>
    </div>
    </div>
    </Router>
    );
  }
}

export default App;
