
import React, { Component } from 'react';
import {Image} from 'react-bootstrap';
import $ from 'jquery';
import { Card, CardText, CardBody,CardTitle} from 'reactstrap';
import StarRatingComponent from 'react-star-rating-component';
 //import PieChart from 'react-simple-pie-chart';


import {Pie} from 'react-chartjs-2';

class Companydetail extends Component {
 constructor(props) {
      super(props);
      this.state={companyData:{},
      rating: 1,
            dataPie: {
            labels: ["A", "B", "C", "D", "E"],
            datasets: [
              {
                data: [50, 200, 10, 70, 60 ],
                backgroundColor: [
                  "#F7464A",
                  "#46BFBD",
                  "#FDB45C",
                  "#949FB1",
                  "#4D5360",
                  // "#ac64ad"
                ],
                hoverBackgroundColor: [
                  "#FF5A5E",
                  "#5AD3D1",
                  "#FFC870",
                  "#A8B3C5",
                  "#616774",
                  // "#da92db"
                ]
              }
            ]
          }
      }
  }

 

  onStarClick(nextValue, prevValue, name) {
    this.setState({rating: nextValue});
  }
   getCompanyData(){
    $.ajax({
      url:'/companyDetail.json',
      dataType:'json',
      cache: false,
      success: function(data){
        this.setState({companyData: data});
      }.bind(this),
      error: function(xhr, status, err){
        console.log(err);
        alert(err);
      }
    });
  }

  componentDidMount(){
    this.getCompanyData();
  }


  render() {
  const { rating } = this.state;
  if(this.state.companyData.main){
   var name = this.state.companyData.main.name;
    var description = this.state.companyData.main.description;
 }
 if(this.state.companyData.form){
      var website = this.state.companyData.form.website;
      var phone = this.state.companyData.form.phone;
      var email = this.state.companyData.form.email;
      var year_founded = this.state.companyData.form.year_founded;
      var size = this.state.companyData.form.size;
      var certification = this.state.companyData.form.certification;
      var head_quaters = this.state.companyData.form.head_quaters;
      var address1 = this.state.companyData.form.address1;
      var address2 = this.state.companyData.form.address2;
      var country = this.state.companyData.form.country;
 }
if(this.state.companyData.Accolades_recognitions){
   var Accolades_recognitions = this.state.companyData.Accolades_recognitions.map(function(Accolades_recognitions){
        var image1 = './images/'+Accolades_recognitions.image;
        })
}

 
    return (
        <div className="container">
         <div className="col-md-12" style={{marginTop:'5%',marginBottom:'3%'}}>
          <div className="col-md-7 companydetail-logo-firm">
          
          <Image src="./images/companylogo.png" className="" /><h2 className="frimname" style={{marginTop:'-8%',marginLeft:'39%',fontSize:'42px'}}>{name}</h2>
          <h2>5.0
          <StarRatingComponent 
          name="rate1" 
          starCount={5}
          value={5}
          onStarClick={this.onStarClick.bind(this)}/>
               10 Reviews
          </h2>
            <p style={{textAlign:'justify'}}>{description}</p>
             <br/>
            <h3> Accolades & recognitions </h3>
              <br/>
            <div className="col-md-12" style={{marginLeft:'-4%'}}>
              <div className="col-md-3">
                <Image src="./images/logo.png" className="" />
              </div>
              <div class="col-md-3">
                <Image src="./images/logo.png" className="" />
              </div>
              <div class="col-md-3">
                <Image src="./images/logo.png" className="" />
              </div>
              <div class="col-md-3">
                <Image src="./images/logo.png" className="" />
              </div>
            </div>
          </div>
          <div className="col-md-5 form-div-companydetail">
           <form>
            <div style={{marginLeft:'5%'}}>
            <label style={{color:'#999'}}>WEBSITE:</label>
            <br/>
            {website}
            <hr style={{margin:'0px',padding:'0px'}} />
            </div>

            <div style={{marginLeft:'5%'}}>
            <label style={{color:'#999'}}>PHONE:</label>
            <br/>
            {phone}
             <hr style={{margin:'0px',padding:'0px'}} />
            </div>

            <div style={{marginLeft:'5%'}}>
            <label style={{color:'#999'}}>EMAIL:</label>
            <br/>
            {email}
             <hr style={{margin:'0px',padding:'0px'}} />
            </div>

            <div style={{marginLeft:'5%'}}>
            <label style={{color:'#999'}}>YEAR FOUNDED:</label>
            <br/>
            {year_founded}
             <hr style={{margin:'0px',padding:'0px'}} />
            </div>

             <div style={{marginLeft:'5%'}}>
            <label style={{color:'#999'}}>SIZE:</label>
            <br/>
            {size}
             <hr style={{margin:'0px',padding:'0px'}} />
            </div>
            
            <div style={{marginLeft:'5%'}}>
            <label style={{color:'#999'}}>CERTIFICATION:</label>
            <br/>
             {certification}
             <hr style={{margin:'0px',padding:'0px'}} />
            </div>

             <div style={{marginLeft:'5%'}}>
            <label style={{color:'#999'}}>HEADQUATERS:</label>
            <br/>
             {head_quaters}
             <hr style={{margin:'0px',padding:'0px'}} />
            </div>
            
             <div style={{marginLeft:'5%'}}>
            <label style={{color:'#999'}}>OTHER LOCATIONS:</label>
            <br/>
             {address1}<br />
             {address2}<br />
             {country}
             <hr style={{margin:'0px',padding:'0px'}} />
            </div>
           </form>
          </div>
        </div>

        <div className="row">
        <div className="col-md-12">
        <div className="col-md-6 chart-div chart-div-left">
         <h3 className="text-center" style={{fontSize:'30px'}}> Firm expertise </h3>
        <div className="col-md-6" style={{marginBottom:'6%'}}>
        <p className="text-center"> Industry expertise </p>
      

         <Pie data={this.state.dataPie} options={{ responsive: true }} height='200' width='200'  />

        </div>
        <div className="col-md-6" style={{marginBottom:'6%'}}>
        <p class="text-center"> Service area </p>
            <Pie data={this.state.dataPie} options={{ responsive: true }} height='200' width='200'  />         
        </div>
        </div>
        <div className="col-md-6 chart-div chart-div-right">
         <h3 className="text-center" style={{fontSize:'30px'}}> Firm focus </h3>
         <div className="col-md-6" style={{marginBottom:'6%'}}>
        <p class="text-center"> Client size </p>
          <Pie data={this.state.dataPie} options={{ responsive: true }} height='200' width='200'  />
        </div>
         <div className="col-md-6" style={{marginBottom:'6%'}}>
         <p class="text-center"> Client stage </p>
            <Pie data={this.state.dataPie} options={{ responsive: true }} height='200' width='200'  />
        </div>
        </div>
        </div>
        </div>

        
        <div className="col-md-12" style={{boxShadow: '1px 1px 2px #eae6e6, 0 0 25px #e0e0e0, 0 0 5px #c3bdbd', marginTop:'5%'}}>

          <div className="company-review-title-div">
            <h2> Review: 5.0  
            <StarRatingComponent 
                    name="rate1" 
                    starCount={5}
                    value={5}
                    onStarClick={this.onStarClick.bind(this)}
                   /> 10 Reviews</h2>
          </div>

          <div class="col-md-12 clnt-img">
            <div class="col-md-4 background-img-div1">
              <h3 class="text-center resp-cmp-deatil" style={{padding: '1%'}}>Client background</h3>
              <p className="cmp-client-detail">Industry:</p>
              <p className="cmp-client-detail">Size:</p>
              <p className="cmp-client-detail">Stage:</p>
            </div>
            <div class="col-md-4 background-img-div2">
              <h3 class="text-center resp-cmp-deatil" style={{padding: '1%'}}>Project background</h3>
              <p className="cmp-client-detail">Service(s):</p>
              <p className="cmp-client-detail">Completion:</p>
              <p className="cmp-client-detail">Project length:</p>
              <p className="cmp-client-detail">Cost:</p>
            </div>
            <div class="col-md-4 background-img-div3">
              <h3 class="text-center resp-cmp-deatil" style={{padding: '1%'}}>Review details</h3>
              <p className="cmp-client-detail">Overall: <StarRatingComponent 
                  name="rate1" 
                  starCount={5}
                  value={5}
                  onStarClick={this.onStarClick.bind(this)}
              /></p>
              <p className="cmp-client-detail">Quality:5.0 Schedule : 5.0</p>
              <p className="cmp-client-detail">Cost :5.0</p>
              <p className="cmp-client-detail">Highlight : Loreum ipsum<br/>
              amet,consectetur<br/>
              adipiscing elit.
              </p>
            </div>
          </div>

          <div className="company-see-more">
            <a href="#"><h3 className="text-center" style={{marginBottom:'2%'}}> See more </h3></a>
          </div>
          </div>
        
      
      </div>
    );
}
  }


export default Companydetail;
