import React from 'react';
import "react-datepicker/dist/react-datepicker.css";
import './reg.css';
import DatePicker from "react-datepicker";
import {Col,DropdownButton,MenuItem,ButtonToolbar,Row,Grid,Button,FormGroup,FormControl,ControlLabel} from 'react-bootstrap';

class Reference extends React.Component {
   constructor() {
    super();
    
  }


render() {
    return (
      <div className="container-fluid">
      <div className="row">
      <div className="col-md-12 col-sm-12 col-lg-12 col-xs-12" >
       <form >
      <label>Your Firm Name</label>
      <input type="text" name="firmname" className="form-control" placeholder="ABC firm"/>

      <label>Client name</label>
      <input type="text" name="name" className="form-control" placeholder=""/>

       <label>Industry</label>
      <select className="form-control">
      <option value="">Enter Industry Name</option>
     <option value="volvo">Volvo</option>
     <option value="saab">Saab</option>
     <option value="mercedes">Mercedes</option>
     <option value="audi">Audi</option>
     </select>   

      <label>Annual Revenue</label>
      <input type="text" name="revenue" className="form-control" placeholder=""/> 

       <label>Client stage</label>
      <select className="form-control">
      <option value=""></option>
     <option value="volvo">Volvo</option>
     <option value="saab">Saab</option>
     <option value="mercedes">Mercedes</option>
     <option value="audi">Audi</option>
     </select>   

     <label>Service(s) provided</label>
      <input type="text" name="service" className="form-control" placeholder=""/>

      <label>Total fees</label>
      <input type="text" name="fees" className="form-control" placeholder=""/>

      <div className="row">
      <div className="col-md-12">
      <div className="col-md-6 ">
      <label>Project length</label>
      <select className="form-control ">
      <option value="">Months</option>
     <option value="volvo">Volvo</option>
     <option value="saab">Saab</option>
     <option value="mercedes">Mercedes</option>
     <option value="audi">Audi</option>
     </select>   
     </div>
     </div>
     </div>
       
     <div className="row">
     <div className="col-md-12">
    
     <div className="col-md-6">
      <label>Completion date</label>
      <select className="form-control ">
      <option value="">MM</option>
     <option value="volvo">Volvo</option>
     <option value="saab">Saab</option>
     <option value="mercedes">Mercedes</option>
     <option value="audi">Audi</option>
     </select> 
     </div> 
       <div className="col-md-6 ">
      <label></label>
      <select className="form-control ">
      <option value="">YY</option>
     <option value="volvo">Volvo</option>
     <option value="saab">Saab</option>
     <option value="mercedes">Mercedes</option>
     <option value="audi">Audi</option>
     </select> 
     </div> 
    </div>
    
     </div>
       

        <label>Reference name</label>
      <input type="text" name="rname" className="form-control" placeholder=""/>

      <label>Reference e-mail</label>
      <input type="text" name="email" className="form-control" placeholder=""/>

       <FormGroup controlId="formControlsTextarea" >
      <ControlLabel>Comments</ControlLabel>
      <FormControl className="textarea" componentClass="textarea"  name ="comment" />
      </FormGroup>

      <div className="col-md-12">
      <div className="col-md-6 ">
      <Button type="button"  bsStyle="warning  ref-add" bsSize="large">Add More</Button>
      </div>
       <div className="col-md-6">
       <Button type="button"  bsStyle="success ref-finish " bsSize="large" > Finish</Button>
      
      </div>
      </div>
      </form>
     </div>
     </div>
     </div>

   );
}
}
export default Reference;