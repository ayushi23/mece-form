import React, { Component } from 'react';
import axios from 'axios';
import {Navbar, Grid, Row, Col, Form, FormGroup, ControlLabel, FormControl, Button, Nav, MenuItem, NavDropdown, NavItem} from 'react-bootstrap';

const API_URL = 'http://dummy.restapiexample.com/api/v1/employees';

class Autocomplete extends Component {

  token = null;
  state = {
    query: "",
    people: []
  };

  onChange = e => {
    const { value } = e.target;

    this.setState({
      query: value
    });

    this.search(value);
  };

  handleBlur = e => {
    this.setState({ people: [] });
  };

  search = query => {

    const url = `http://dummy.restapiexample.com/api/v1/employees?search=${query}`;
    const token = {};
    this.token = token;

    fetch(url)
      .then(results => results.json())
      .then(data => {
        if (this.token === token) {
          this.setState({ people: data});
        }
        else
        {
          this.setState({ people: [] });
        }
      });
  };



  componentDidMount() {
    this.search("");
  }

  render() {
    return (
        <Col xs={12}>
            <form>
                <input
                  type="text"
                  className="search-box"
                  placeholder="Search for Dynamic"
                  onChange={this.onChange}
                  onBlur={this.handleBlur}
                />
                {this.state.people.map(person => (
                  <ul key={person.employee_name}>
                    <li>{person.employee_name}</li>
                  </ul>
                ))}
            </form>
        </Col>
    );
  }

}
export default Autocomplete;