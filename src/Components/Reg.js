import React from 'react';
import "react-datepicker/dist/react-datepicker.css";
import './reg.css';
import DatePicker from "react-datepicker";
import {Col,DropdownButton,MenuItem,ButtonToolbar,Row,Grid,Button,FormGroup,FormControl,ControlLabel} from 'react-bootstrap';

class Reg extends React.Component {
   constructor() {
    super();
    
  }


render() {
    return (
  
      <div className="container-fluid">
      <div className="row">
    
    <div className="col-md-12 col-sm-12" >
   <div className="col-md-6 col-sm-6" >
     <div className="register">
      <div className="formheader" >
        <h3  style={{padding:'23px',fontSize:'30px'}}>Personal Information</h3>
        
        </div>
        <form >
        <label className="textsize-contact">Name*</label>
        <input type="text" name="username" className="form-control" placeholder="Enter Name"/>
        
        <label className="textsize-contact">Email*</label>
        <input type="text" className="form-control" name="emailid"  placeholder="Enter Email Address" />
        
        <label className="textsize-contact">Phone*</label>
        <input type="text" name="mobileno" className="form-control"  placeholder="+91" />
        

        <span className="note">Note:Personal Information will NOT be shared with potential firms</span>
       
        </form>
        </div>
   
    </div>
      

       
       <div className="col-md-6 col-sm-6" >
      <div className="register1">
      <div className="formheader" >
        <h3  style={{padding:'23px',fontSize:'30px'}}>Company Information</h3>
       
        </div>
        <div>
       <form>
     <label className="textsize-contact">Company Name*</label>
      <input type="text" name="username" className="form-control" placeholder="Enter Company Name"/>
      <label className="textsize-contact">Industry*</label>
      <select className="form-control">
      <option value="Enter Industry Name">Enter Industry Name</option>
     <option value="volvo">Volvo</option>
     <option value="saab">Saab</option>
     <option value="mercedes">Mercedes</option>
     <option value="audi">Audi</option>
     </select> 
      <label className="textsize-contact">Maturity*</label>
      <select className="form-control">
      <option value="Maturity">Maturity</option>
     <option value="volvo">Volvo</option>
     <option value="saab">Saab</option>
     <option value="mercedes">Mercedes</option>
     <option value="audi">Audi</option>
     </select> 
      <label className="textsize-contact">Size*</label>
      <select className="form-control">
      <option value="#employees">#employees</option>
     <option value="volvo">Volvo</option>
     <option value="saab">Saab</option>
     <option value="mercedes">Mercedes</option>
     <option value="audi">Audi</option>
     </select> 
     </form>
    </div>
    </div>
    </div>
    </div>
    </div>

    <div className="rowproject" >
     
     <div className="col-md-12 col-sm-12 col-lg-12 col-xs-12" >
     <div className="register2">
     <h3 className="formheader1">Project Information</h3>
      <hr className="projectborder"/>
      <form>
      <div className="row">
      
      <div className="col-md-6 col-sm-6" >
      <div className="form-group">
      <label className="textsize-contact">Service(s)</label>
      <input type="text" name="service" className="form-control" placeholder="From Previous Page" />
      </div>
      </div>
       
       <div className="col-md-6 col-sm-6" >
        <div className="form-group">
      <label className="textsize-contact">Response deadline*</label>
      <input type="date" placeholder="Calendar pop-up" className="form-control"/>
      </div>
      </div>
      </div>

      <FormGroup controlId="formControlsTextarea" >
      <ControlLabel className="textsize-contact" >Objective(s)*</ControlLabel>
      <FormControl className="textarea" componentClass="textarea"  name ="objective" placeholder="Textarea" />
      </FormGroup>
     
      <FormGroup controlId="formControlsTextarea" >
      <ControlLabel className="textsize-contact">Scope*</ControlLabel>
      <FormControl className="textarea" componentClass="textarea"  name ="scope"  />
      </FormGroup>

      <FormGroup controlId="formControlsTextarea" >
      <ControlLabel className="textsize-contact">Problem Statement*</ControlLabel>
      <FormControl className="textarea" componentClass="textarea" name="problem" placeholder="Provide any background regarding the project to help narrow down the vendor options" />
      </FormGroup>

       <FormGroup controlId="formControlsTextarea" >
      <ControlLabel className="textsize-contact">Additional Information</ControlLabel>
      <FormControl className="textarea" componentClass="textarea" name="problem_statement"  placeholder="Provide any additional information that will help potential firms in understanding your bussiness need" />
      </FormGroup>
      <div className="row" style={{padding: '0px 0px 0px 0px',width: 'unset'}}>
    
      
      <div className="col-md-6 col-sm-6" >
      <label className="textsize-contact">Timeline</label>
      <input type="text" name="timeline" className="form-control" placeholder="Approx.duration for the project"/>
      </div>
      
      <div className="col-md-6 col-sm-6" >
      <div className="form-group">
      <label className="textsize-contact">Budget</label>
      <input type="text" name="budget" className="form-control" placeholder="Approx.amount alloted for the project"/>
      </div>
      </div>
      </div>
      </form>
      </div>
     </div>
   </div>


    <div className="success cnt-success ">
     <Button type="button" className="btn-contact" bsStyle="success" bsSize="large" style={{paddingLeft:'50%',paddingRight:'50%', marginTop: '22%'}}> Submit</Button>
    </div>

    <div class="referesh-submission">
<div className="col-md-12 col-sm-12">
<h3 class="referesh">Click here to contact us for questions.</h3>
<div className="click">
<Button type="button" className="button" >Contact us</Button>
</div>
</div>
</div>

   </div>

);
}
}
export default Reg;