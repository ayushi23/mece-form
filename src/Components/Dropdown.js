import React, { Component } from 'react';




// class Dropdown extends React.Component {
//   constructor() {
//     super();
//     this.state = {
//       optionsdata : [
//          {key:'101',value:'Lion'},
//          {key:'102',value:'Giraffe'},
//          {key:'103',value:'Zebra'},
//          {key:'104',value:'Hippo'},
//          {key:'105',value:'Penguin'}
//        ]
//     }
//   }
//   handleChange = (e) => {
//     console.log(e.target.value);
//     var value = this.state.optionsdata.filter(function(item) {
//       return item.key == e.target.value
//     })
//     {this.state.optionsdata}
//     console.log(value[0].value);
//   }
//   render() {
//     return (

//       <select onChange={this.handleChange}>
//         {this.state.optionsdata.map(function(data, key){  return (
//           <option key={key} value={data.key}>{data.value}</option> )
//         })}
//       </select>

//     )
//   }
// }
// export default Dropdown;




class Dropdown extends React.Component {

  constructor(props){
    super(props)
    this.state = {
      result: ''
    };
  }
  
  handleSelectChange = (event) => {
    this.setState({
      result: event.target.value
    })
  }

  render() {
    return (
      <div>
      <h2>Static Dropdown</h2>
      <select onClick={this.handleSelectChange}>
         <option value="select">Select</option>
          <option value="Clang">Clang</option>
          <option value="C++">C++</option>
          <option value="C#">C#</option>
          <option value="Php">Php</option>
          <option value="java">Java</option>
      </select>
      {this.state.result}
      </div>
    );
  }
}
export default Dropdown;
