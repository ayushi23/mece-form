import React from 'react';
import { Button,Nav, Glyphicon,Col,Thumbnail,Grid,Row,Navbar} from 'react-bootstrap';
import './reg.css';
import $ from 'jquery';


import Pagination from "react-js-pagination";
class Sidebar extends React.Component {
   constructor() {
        super();
 
    }



componentWillMount() {
 
}

render() {

    return (
          <div className="container-fluid">
          <div className="row">
           <Col md={12} sm={12}>
           <Col md={3}>
           <h2 className="text-center">Refine Search</h2>
            <input type="text" className="form-control empty" placeholder='&#61442;' style={{position:'absolute', width:'90%'}}/>
           <hr/>
              <section className="sidebar">
             <div class="navbar-header">
             <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#sidebar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
           </div>

              <div class="collapse navbar-collapse" id="sidebar">
                     <ul className="sidebar-menu tree" data-widget="tree">
                    	<li className="treeview">
                        <a href="javascript:void(0)">
                            
                            <span className="sidetopic">Service Area</span>
                            <span className="pull-right-container">
                            <i className="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul className="treeview-menu">
                            <li><a href="#">California 1 <span className='treeview-menu-span'> 1100 </span></a></li>
                            <li><a href="#">California 2 <span className='treeview-menu-span'> 112 </span></a></li>
                            <li><a href="#">California 3 <span className='treeview-menu-span'> 101 </span></a></li>
                            <li><a href="#">California 4 <span className='treeview-menu-span'> 111 </span></a></li>
                        </ul>
                        </li>
                        

                        <li className="treeview">
                        <a href="javascript:void(0)">
                           <span className="sidetopic">Service(s)</span>
                            <span className="pull-right-container">
                            <i className="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        </li>
                        


                        <li className="treeview">
                        <a href="javascript:void(0)">
                            
                            <span className="sidetopic">Location</span>
                            <span className="pull-right-container">
                            <i className="fa fa-angle-left pull-right"></i>
                            </span>
                            </a>
                        </li>


                        <li className="treeview">
                        <a href="javascript:void(0)">
                           
                            <span className="sidetopic">Company Industry</span>
                            <span className="pull-right-container">
                            <i className="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        </li>
                        
                        <li className="treeview">
                          <a href="javascript:void(0)">
                           <span className="sidetopic">Company Revenue</span>
                            <span className="pull-right-container">
                            <i className="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                       
                        </li>
                          <li className="treeview">
                          <a href="javascript:void(0)">
                          
                            <span className="sidetopic">Company Stage</span>
                            <span className="pull-right-container">
                            <i className="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                       <ul className="treeview-menu">
                            <li><a href="#">Seed & Development</a></li>
                            <li><a href="#" >Start-up</a></li>
                            <li><a href="#" >Growth</a></li>
                            <li><a href="#" >Established</a></li>
                            <li><a href="#" >Expension</a></li>
                            <li><a href="#" >Mature</a></li>
                            <li><a href="#">Exit</a></li>
                        </ul>
                        </li>

                        <li className="treeview">
                        <a href="javascript:void(0)">
                          <span className="sidetopic">Resource(s) need</span>
                            <span className="pull-right-container">
                            <i className="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                       
                        </li>

                        <li className="treeview">
                        <a href="javascript:void(0)">
                           <span className="sidetopic">Certification need</span>
                            <span className="pull-right-container">
                            <i className="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                      </li>
                 
                       
                    </ul>

                     <div style={{marginTop:'15%'}}>
                     <Button type="button"  bsStyle="warning" style={{width: '38%'}}>Apply</Button>
                      <Button type="button" bsStyle="danger" style={{marginLeft: '19%',width: '38%'}}>Reset</Button>
                     </div>
                    
                    
                     </div>
                </section>
           </Col>

           <Col md={9}>
           <h2 className="text-center">Select Up to 5 Contact</h2>
           <p className="sidebartext text-center" >contact firms with one click </p>
           <hr className="contact"/>
           <div className="row">
           <Col md={4}>
            
        
            <Thumbnail src="./images/logo.png" alt="242x200">
            <h5>Company Name <span className="cal pull-right">California</span></h5>
            <p className="text sidebar-p">www.company.com<span className="number pull-right">1800 111 1110</span></p>
            <hr/>
             <p>
              <a href = "/Companydetail" ><u className="text">More Details</u></a> 
              &nbsp;
             <Button type="button" className="applied pull-right" bsStyle="warning">Applied</Button>
            </p>
            </Thumbnail>
           </Col>
            <Col md={4}>
             <Thumbnail src="./images/logo.png" alt="242x200">
            <h5>Company Name <span className="cal pull-right">California</span></h5>
            <p className="text sidebar-p">www.company.com<span className="number pull-right">1800 111 1110</span></p>
            <hr/>
             <p>
            <a href = "/Companydetail" ><u className="text">More Details</u></a> 
              &nbsp;
             <Button  type="button" className="applied pull-right" bsStyle="warning">Applied</Button>
            </p>
            </Thumbnail>
           </Col>
            <Col md={4}>
              <Thumbnail src="./images/logo.png" alt="242x200">
            <h5>Company Name <span className="cal pull-right">California</span></h5>
            <p className="text sidebar-p">www.company.com<span className="number pull-right">1800 111 1110</span></p>
            <hr/>
             <p>
            <a href = "/Companydetail" ><u className="text">More Details</u></a> 
              &nbsp;
            <Button  type="button" className="applied pull-right" bsStyle="warning">Applied</Button>
            </p>
            </Thumbnail>
           </Col>
            <Col md={4}>
              <Thumbnail src="./images/logo.png" alt="242x200">
            <h5>Company Name <span className="cal pull-right">California</span></h5>
            <p className="text sidebar-p">www.company.com<span className="number pull-right">1800 111 1110</span></p>
            <hr/>
             <p>
            <a href = "/Companydetail" ><u className="text">More Details</u></a> 
              &nbsp;
             <Button className="default appl-now pull-right">Apply Now</Button>
            </p>
            </Thumbnail>
           </Col>
            <Col md={4}>
             <Thumbnail src="./images/logo.png" alt="242x200">
            <h5>Company Name <span className="cal pull-right">California</span></h5>
            <p className="text sidebar-p">www.company.com<span className="number pull-right">1800 111 1110</span></p>
            <hr/>
             <p>
            <a href = "/Companydetail" ><u className="text">More Details</u></a> 
              &nbsp;
             <Button className="default appl-now pull-right">Apply Now</Button>
            </p>
            </Thumbnail>
           </Col>
            <Col md={4}>
              <Thumbnail src="./images/logo.png" alt="242x200">
            <h5>Company Name <span className="cal pull-right">California</span></h5>
            <p className="text sidebar-p">www.company.com<span className="number pull-right">1800 111 1110</span></p>
            <hr/>
             <p>
            <a href = "/Companydetail" ><u className="text">More Details</u></a> 
              &nbsp;
             <Button className="default appl-now pull-right">Apply Now</Button>
            </p>
            </Thumbnail>
             </Col>
            <Col md={4}>
              <Thumbnail src="./images/logo.png" alt="242x200">
            <h5>Company Name <span className="cal pull-right">California</span></h5>
            <p className="text sidebar-p">www.company.com<span className="number pull-right">1800 111 1110</span></p>
            <hr/>
             <p>
            <a href = "/Companydetail" ><u className="text">More Details</u></a> 
              &nbsp;
             <Button className="default appl-now pull-right">Apply Now</Button>
            </p>
            </Thumbnail>
            </Col>
            <Col md={4}>
             <Thumbnail src="./images/logo.png" alt="242x200">
            <h5>Company Name <span className="cal pull-right">California</span></h5>
            <p className="text sidebar-p">www.company.com<span className="number pull-right">1800 111 1110</span></p>
            <hr/>
             <p>
            <a href = "/Companydetail" ><u className="text">More Details</u></a> 
              &nbsp;
            <Button type="button" className="applied pull-right" bsStyle="warning">Applied</Button>
            </p>
            </Thumbnail>
           </Col>
            <Col md={4}>
             <Thumbnail src="./images/logo.png" alt="242x200">
            <h5>Company Name <span className="cal pull-right">California</span></h5>
            <p className="text sidebar-p">www.company.com<span className="number pull-right">1800 111 1110</span></p>
            <hr/>
             <p>
            <a href = "/Companydetail" ><u className="text">More Details</u></a> 
              &nbsp;
             <Button type="button" className="applied pull-right" bsStyle="warning">Applied</Button>
            </p>
            </Thumbnail>
           </Col>
           </div>
           <div className="text-center" >
         
           <Pagination
              hideNavigation
              pageRangeDisplayed={5}
              activePage={1}
              itemsCountPerPage={9}
              totalItemsCount={45} />
           </div>
           </Col>
           </Col>
           <div className="text-center" >
             <Button className="sidebar-contact" bsStyle="primary" style={{marginLeft:'26%',marginTop: '5%',width: '14%',borderRadius: '7%',fontSize: '24px'}}>Contact Firms</Button>
             </div>
          </div>
          </div>
    	);
    }
}
export default Sidebar;