import React from 'react';
import {Col,Image, Button, ButtonGroup} from 'react-bootstrap';
import SocialMediaIcons from 'react-social-media-icons';
const socialMediaIcons = [
  {
    url: 'https://fb.com',
    className: 'fab fa-facebook-f fab-facebook',
  },
  {
    url: 'https://twitter.com',
    className: 'fab fa-twitter fab-twitter',
  },
  {
    url: 'https://plus.google.com/discover',
    className: 'fab fa-google-plus-g fab-google',
  },
];
class Blog extends React.Component {


render() 
{
    return (

    	  <div className="container">
    	     <div className="row">
    	      <Col md={12} sm={12}>
             

    	     <h1 style={{fontWeight:'bold'}}>
    	      Lorem Ipsum is simply dummy
    	      text of the printing and text of the printing typesetting industry.
    	      </h1>
    	      <p style={{color:"grey"}}><i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;
            November 12,2018 by <span style={{color:'black'}}>Matthew Johns</span></p>
    	     <p className="blogtext-p">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software .</p>

    	     <div className="text-center">
              <Image src="./images/Group 5.jpg" rounded  style={{width:'60%'}}/>
             </div>
             <div>
             <h2 className="blogtext" style={{fontWeight:'bold'}}>Lorem Ipsum is simply dummy text of the printing</h2>
             <p className="blogtext-p">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software .</p>
             </div>
             <div>
             <h2 className="blogtext" style={{fontWeight:'bold'}} >Lorem Ipsum is simply dummy text of the printing</h2>
             <p className="blogtext-p">It is a long established fact that a reader will be discracted by the readble.</p>
             <Image src="./images/left-quote-new.jpeg" style={{width:'50px'}}/>
             <i><p style={{marginLeft:'7%', fontSize:'25px', marginTop:'-2%'}}>
"            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s."</p></i>
             </div>
             <hr style={{width:'6%',marginLeft:'45%'}}/>
             <div >
             <h3 className="text-center" style={{fontWeight:'bold'}}>SHARE THIS ARTICLE</h3>
             <div >
          		 <SocialMediaIcons
                icons={socialMediaIcons}
                iconSize={'1.3em'}
                iconColor={'#495056'}
              />
              </div>
             </div>
    	      </Col>
    	     </div>
    	  </div>
    	);
}
}
export default Blog;

