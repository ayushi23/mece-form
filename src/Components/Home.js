import React, { Component } from 'react';
import { Link } from "react-router-dom";
import Select from 'react-select';
import { colourOptions } from '../data';
import { Card, CardImg, CardText, CardBody,CardTitle, CardSubtitle } from 'reactstrap';
import {Navbar, Col, Form, FormGroup, ControlLabel, FromGroup, Nav, MenuItem, NavDropdown, NavItem, FormControl, Button, Image , ButtonGroup} from 'react-bootstrap';
import {ReactSelectize, SimpleSelect, MultiSelect} from 'react-selectize';

class Home extends Component {

  render() {
    return (
      <div class="container-fluid">
        
        <div className="banner-form-div ">
        <Form className="header-search-form" >
          <Col sm={6} md={4}>
            <MultiSelect
            className="select-one"
                placeholder = "Keyword Search"
                options = {["Php", "asp.net", "android", "ios"].map(
                  fruit => ({label: fruit, value: fruit})
                )}
            />
          </Col>

          <Col sm={6} md={4}>
           <MultiSelect
           className="select-two select-two-media"
                placeholder = "Services"
                options = {["Php", "asp.net", "android", "ios"].map(
                  fruit => ({label: fruit, value: fruit})
                )}  
            />
          </Col>
          <Col sm={6} md={4} className="loc-exp-div">
           <ButtonGroup className="btn-grp-location">
              <Button className="btn-lg btn-cross"><Image src="./images/gps.png" style={{width:'22px'}}/></Button>
              <Button className="btn-location btn-lg" bsStyle="warning" >Location</Button>  
          </ButtonGroup>
           
            <Button className="btn-explore btn-lg" bsStyle="success" >Explore</Button>
          </Col>  
          <Col sm={12} md={12} style={{marginTop:'10px'}}>
            <Col sm={6} md={6} className="guided-div">
              <Button className="btn-guided btn-lg" bsStyle="info" >Guided search</Button>
            </Col>  
            <Col sm={6} md={6}>
              <Button className="btn-demo btn-lg" bsStyle="primary">Demo</Button>
            </Col>
          </Col>
        </Form>
        
      </div>
        <div class="col-md-offset-2 col-md-8 col-md-offset-2" style={{zIndex:'-1'}}>
          <div className="row step-div">
            <h1>MECE Inc. Works in FIVE Steps</h1>
            <Image src="./images/Steps.png" className="stepimg" />
          </div>
        </div>
       
        <div class="col-md-12" style={{marginTop: '-8%'}}>
          <div className="row keyfeature-div">
          <br/>
            <h1>Key Benefits</h1>
            <hr style={{width:'10%',marginLeft:'45%'}}/>
            <br />
            <div class="col-md-12 text-center">
              <div class="col-md-2 col-md-offset-1 keybeni keybeni-first">
                <Image style={{width:'45%'}} src="./images/you charge.png" />
                <h4 style={{fontWeight:'bold',color: '#1b3f69',fontSize:'25px'}}>You are in charge</h4><br />
                <p style={{color: '#1b3f69',fontSize:'18px'}} className="resp-keybenifits"> Search for vendor that fit 
                 your need. Set the objective 
                 and desired results.</p>
              </div>
              <div class="col-md-2 col-md-offset-1 keybeni" style={{marginLeft:'5%'}}>
                <Image style={{width:'45%'}} src="./images/connect 1 portal.png" />
                <h4 style={{fontWeight:'bold',color: '#1b3f69',fontSize:'25px'}}>You connect in one portal. </h4><br />
                <p style={{color: '#1b3f69',fontSize:'18px',}} className="resp-keybenifits">Select potential vedors to receive responses from with 
                a click of a button.</p>
              </div>
              <div class="col-md-2 col-md-offset-1 keybeni" style={{marginLeft:'5%'}}>
                <Image style={{width:'45%'}} src="./images/get to compare.png" />
                <h4 style={{fontWeight:'bold',color: '#1b3f69',fontSize:'25px'}}>You get to compare.</h4><br />
                <p style={{color: '#1b3f69',fontSize:'18px'}} className="resp-keybenifits">Review the vendor responses 
                 and rate them.</p>
              </div>
              <div class="col-md-2 col-md-offset-1 keybeni" style={{marginLeft:'5%'}}>
                <Image style={{width:'45%'}} src="./images/get choose.png" />
                <h4 style={{fontWeight:'bold',color: '#1b3f69',fontSize:'25px'}}>You get to choose.</h4><br />
                <p style={{color: '#1b3f69',fontSize:'18px'}} className="resp-keybenifits">Interview and select vendor. </p>
              </div>
            </div>
          </div>
        </div>


        <div class=" col-md-offset-1 col-md-10 col-md-offset-1 value-proposition-div">
          <div class="row">
            <div class="col-md-12">
              <h1>Value proposition </h1><br />
              <div class="col-md-4 ">
              <Card className="valuepropostion">
                    <CardImg top width="100%" src="./images/Rounded Rectangle 1 copy 4.png" alt="Card image cap" />
                    <CardBody>
                      <CardText>Comprehensive view of 
                      providers driven by need,
                       quality and geography. </CardText>
                    </CardBody>
                  </Card>
              </div>
               
               <div class="col-md-4 ">
             <Card className="valuepropostion1">
                    <CardImg top width="100%" src="./images/Rounded Rectangle 1 copy 5.png" alt="Card image cap" />
                    <CardBody>
                      <CardText>Customized options based on
                       proprietary information driven 
                       on Artificial inteliigence (AI)
                        technology. </CardText>
                    </CardBody>
                  </Card>
              </div>
             
              <div class="col-md-4 ">
             <Card className="valuepropostion">
                    <CardImg top width="100%" src="./images/Rounded Rectangle 1 copy 6.png" alt="Card image cap" />
                    <CardBody>
                      <CardText>Value creation from each new 
                      relationship between client and a firm.</CardText>
                    </CardBody>
                  </Card>
              </div>
            </div>
          </div>
        </div>


        <div class="col-md-12 story-about-agency-div" style={{backgroundImage:'url(./images/about.jpg)',backgroundSize: 'cover'}}>
          <h1> Story About Agency </h1>
          <hr style={{width:'6%',marginLeft:'46%'}}/>
          <div class="col-md-12" style={{marginTop:'3%'}}>

          <div class="col-md-4 text-center story-1 ">
              <Image src="./images/1.png" />
              <h4 style={{fontWeight:'bold'}}>Explore options.</h4>
              <p className="stry-1">Search for firms that provide a 
               specific service based on various 
               criteria that fits your need.</p>
          </div>
          <div class="col-md-4 text-center story-1" >
              <Image src="./images/2.png" />
              <h4 style={{fontWeight:'bold'}}>Select firm to contact</h4>
              <p className="stry-2">Using proprietary information, we 
               provide a sorted list of firms that
                best fit yuour business need.
                 select upto five firms to contact.</p>
          </div>
          <div class="col-md-4 text-center story-1">
              <Image src="./images/3.png" />
              <h4 style={{fontWeight:'bold'}}> Review responses and interview</h4>
              <p className="stry-3">Review firm solutions to your business need.
               interview the firm that have compelling 
                solution to your problem within 
                you timeline & budget. </p>
          </div>
          </div>

          <div class="col-md-12 second-line-agency-div">
            <div class="col-md-offset-2 col-md-4 text-center story-1">
              <Image src="./images/4.png" />
              <h4 style={{fontWeight:'bold'}}>Interview and select firm</h4>
              <p className="stry-4">Interview the firms that fit your 
               business need and gather answer  to 
               all your open questions/concerns.
                select the vendors.</p>
            </div>
             <div class="col-md-4 text-center story-1">
                <Image src="./images/5.png" />
                <h4 style={{fontWeight:'bold'}}>Rate firm performance</h4>
                <p className="stry-5">After the project is completed, please 
                 rate the firm on each pillar and provide 
                 any additional comments/feedback.</p>
            </div>
            <div class="col-md-4"></div>
          </div>
        </div>



        <div class=" col-md-offset-1 col-md-10 col-md-offset-1 blog-post-div" style={{marginTop:'-4%'}}>
          <h1> Blog Posts </h1>
          <hr style={{width:'10%',marginLeft:'45%'}}/>
          <h4 class="col-md-offset-2 col-md-8">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</h4>
          <br />

          <div class="col-md-4 blog-div-first">
            <Card>
              <CardImg top width="100%" src="./images/b1.jpg" alt="Card image cap" />
              <div class="blog-post-card-body">
              <CardBody>
              <CardSubtitle><i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;Novermber 12, 2018 by <b>Methew jhones</b> </CardSubtitle>
                <h3 style={{fontWeight:'600'}}>Sunset in venice between building.</h3>
                <CardText style={{color:'#808080'}}>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText>
                <a href="#">Read more</a>
              </CardBody>
              </div>
            </Card>
          </div>

          <div class="col-md-4 blog-div-sec">
            <Card>
              <CardImg top width="100%" src="./images/b2.jpg" alt="Card image cap" />
              <div class="blog-post-card-body">
              <CardBody>
              <CardSubtitle><i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;October 30, 2018 by <b>Richard Jackson</b> </CardSubtitle>
                <h3 style={{fontWeight:'600'}}>Aerial view of village on Mountain Cliff.</h3>
                <CardText style={{color:'#808080'}}>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText>
                <a href="#">Read more</a>
              </CardBody>
              </div>
            </Card>
          </div>

          <div class="col-md-4 blog-div-third">
            <Card>
              <CardImg top width="100%" src="./images/b3.jpg" alt="Card image cap" />
              <div class="blog-post-card-body">
              <CardBody>
                <CardSubtitle><i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;September 27, 2018 by <b>Alex Wesly</b> </CardSubtitle>
                <h3 style={{fontWeight:'600'}}>Lighted concentrate city near on bridge.</h3>
                <CardText style={{color:'#808080'}}>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText>
                <a href="#">Read more</a>
              </CardBody>
              </div>
            </Card>
          </div>
        </div>


        <div class="col-md-12 map-image-div" style={{marginBottom:'-6%'}} >
          <div class="map-image">
            <div class="row">
            <form class=" map-form pull-right">
              <div class="col-md-12">
          
                  <center><h3 className="abc"> Contact Us </h3></center>
                
                  <div class="col-md-6">
                  <h5 style={{fontWeight:'bold'}}>
                  <Image src='./images/call.png' style={{width: '20px'}} />
                  &nbsp; Give us a ring</h5>
                  <p style={{marginLeft: '12%',color:'#a79797'}}>Michel jorden <br /> 
                  +40 6800 7800<br/>
                  Mon-Fri,8:00-22:00
                  </p>

                  </div>
                  <div class="col-md-6">
                  <h5 style={{fontWeight:'bold'}}>
                  <Image src='./images/location.png' style={{width: '20px'}} />&nbsp;
                    Find us at the office</h5>
                   <p  style={{marginLeft: '12%',color:'#a79797'}}>Bid Mihail Kogalniceanu <br /> 
                   nr.8,7652 Bucharest<br/>
                   Texas
                   </p>
                  </div>
                 </div>
                 <div className="col-md-12">
                 <div className="col-md-6">
                  <input style={{minWidth:'0px', padding:'0px'}} type="text" name="fullname" placeholder="Full Name" className="contact-us-text"/>
                      <input style={{minWidth:'0px', padding:'0px'}} type="text" name="email" placeholder="Email" className="contact-us-text"/>
                 </div>
                 <div className="col-md-6">
                  <input style={{minWidth:'0px', padding:'0px'}} type="text" name="companyname" placeholder="Company Name" className="contact-us-text"/>
                      <input style={{minWidth:'0px', padding:'0px'}} type="text" name="phone" placeholder="Phone" className="contact-us-text"/>
                 </div>
                 </div>
                 
                 <div class="col-md-12">
                   <div class="col-md-12">
                      <textarea style={{minHeight:'0px', width:'100%'}} placeholder="Your Message" className="contact-us-text"></textarea><br/>
                   </div>
                 </div>
                 

                 <div class="col-md-12">
                 <div class="col-md-6">
                      <input type="checkbox" />&nbsp;I'm not a robot.
                    </div>
                    <div class="col-md-6">
                      <button class="btn btn-warning snd-msg" style={{marginLeft:'30%', background:'#f7811c', boxShadow: '0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 -5px 10px 0 rgba(0, 0, 0, 0.12)'}}>Send Message</button>
                    </div>
                </div>
              </form>
             
            </div>
          </div>
        </div>
      </div>
     
    );
  }
}

export default Home;
