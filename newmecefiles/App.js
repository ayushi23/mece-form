import React, { Component } from 'react';
import ReactGA from 'react-ga';
import $ from 'jquery';
import './App.css';
import Search from './Components/Search';
import Header from './Components/Header';
import Reg from './Components/Reg';
import Sidebar from './Components/Sidebar';
import Footer from './Components/Footer';
import Companydetail from './Components/Companydetail';
import Demo from './Components/Demo';
import About from './Components/About';
import Blog from './Components/Blog';
import Contact from './Components/Contact';
import Signup from './Components/Signup';
import Login from './Components/Login';
import Home from './Components/Home';
import { BrowserRouter, Route, } from 'react-router-dom';
import {Navbar, Form, Col, FormGroup, ControlLabel, FormControl, Button, Nav, MenuItem, NavDropdown, NavItem, Image} from 'react-bootstrap';
//import Autocomplete from './Components/Autocomplete';
// import Header from './Components/Header';
// import Footer from './Components/Footer';
// import About from './Components/About';
// import Resume from './Components/Resume';
// // import Search from './Components/Search';
// //import Contact from './Components/Contact';
// import Testimonials from './Components/Testimonials';
// import Portfolio from './Components/Portfolio';
import Main from './Components/Main';

class App extends Component {

  constructor(props){
    super(props);
    this.state = {
      foo: 'bar',
      contactName:'',
      contactEmail:'',
      contactSubject:'',
      contactMessage:'',
      resumeData: {}
    };

    ReactGA.initialize('UA-110570651-1');
    ReactGA.pageview(window.location.pathname);

  }

  handleChange (event) {
   console.log(event);
    this.setState( [event.target.name]: event.target.value )
  }

  getResumeData(){
    $.ajax({
      url:'/resumeData.json',
      dataType:'json',
      cache: false,
      success: function(data){
        this.setState({resumeData: data});
      }.bind(this),
      error: function(xhr, status, err){
        console.log(err);
        alert(err);
      }
    });
  }

  componentDidMount(){
    this.getResumeData();
  }

  render() {
    return (
      <div className="App">
        <Main/>

        <Navbar inverse collapseOnSelect fixedTop>
      <Navbar.Header>
        <Navbar.Brand>
          <a href="#brand"><Image src="./images/2.jpg" style={{width:'50px'}}/></a>
        </Navbar.Brand>
        <Navbar.Toggle />
      </Navbar.Header>
      <Navbar.Collapse>
          <Nav className="main-nav" style={{fontWeight:'bold',color:'white'}}>
            <NavItem eventKey={1} href="/Sidebar">
              Explore
            </NavItem>
            <NavItem eventKey={2} href="/Home">
              Demo
            </NavItem>
             <NavItem eventKey={2} href="/Companydetail">
              Companydetail
            </NavItem>
            <NavItem eventKey={2} href="/About">
              About
            </NavItem>
            <NavItem eventKey={2} href="/Blog">
              Blog
            </NavItem>
            <NavItem eventKey={2} href="/Reg">
              Contact
            </NavItem>
          </Nav>
          <Nav pullRight>

            <NavItem eventKey={1} href="/Signup">
            <i class="fa fa-wrench" aria-hidden="true"></i>&nbsp;
              Sign Up
            </NavItem>
            <NavItem eventKey={2} href="/Login">
            <i class="fa fa-user" aria-hidden="true"></i>&nbsp;
              Login
            </NavItem>
          </Nav>
        </Navbar.Collapse>
      </Navbar>

        <BrowserRouter>
         <Route path="/" exact component={Home} />
         </BrowserRouter>
        <BrowserRouter>
           <Route path="/Sidebar" component={Sidebar} />
        </BrowserRouter>
        <BrowserRouter>
           <Route path="/Home" component={Home} />
        </BrowserRouter>
         <BrowserRouter>
           <Route path="/Companydetail" component={Companydetail} />
        </BrowserRouter>
         <BrowserRouter>
           <Route path="/About" component={About} />
        </BrowserRouter>
         <BrowserRouter>
           <Route path="/Blog" component={Blog} />
        </BrowserRouter>
         <BrowserRouter>
           <Route path="/Reg" component={Reg} />
        </BrowserRouter>
         <BrowserRouter>
           <Route path="/Signup" component={Signup} />
        </BrowserRouter>
        <BrowserRouter>
           <Route path="/Login" component={Login} />
        </BrowserRouter>
       
        <Footer />


      </div>
    );
  }
}

export default App;
